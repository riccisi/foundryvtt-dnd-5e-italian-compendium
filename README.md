# FoundryVTT DnD 5e Italian Compendium

## Installation

To install, follow these instructions:

1.  Inside Foundry, select the Game Modules tab in the Configuration and Setup menu.
2.  Click the Install Module button and enter the following URL: https://gitlab.com/riccisi/foundryvtt-dnd-5e-italian-compendium/raw/master/module/module.json
3.  Click Install and wait for installation to complete